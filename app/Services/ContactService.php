<?php

namespace App\Services;

use App\Contact;
use App\Interfaces\ContactRepositoryInterface;


class ContactService
{
    protected $contactRepository;

    /**
     * ContactService constructor.
     *
     * @param ContactRepositoryInterface $contactRepository
     */
    public function __construct(ContactRepositoryInterface $contactRepository)
    {
        $this->contactRepository = $contactRepository;
    }

    public function findByName(string $name): ?Contact
    {
        return $this->contactRepository->findByName($name);
    }

    public function validateNumber(string $number): bool
    {
        // Only PERU mobile numbers
        return preg_match('/^(\+51)?9\d{8}$/', $number);
    }
}
