<?php

namespace App;

use App\Interfaces\CarrierInterface;
use App\Services\ContactService;

/**
 * Class Mobile
 */
class Mobile
{
    /**
     * @var CarrierInterface
     */
    protected $provider;

    /**
     * @var ContactService
     */
    protected $contactService;

    /**
     * Mobile constructor.
     *
     * @param CarrierInterface $provider
     * @param ContactService   $contactService
     */
    public function __construct(CarrierInterface $provider, ContactService $contactService)
    {
        $this->provider       = $provider;
        $this->contactService = $contactService;
    }

    /**
     * @param string $name
     *
     * @return Call|null
     */
    public function makeCallByName($name = ''): ?Call
    {
        if (empty($name)) {
            return null;
        }

        $contact = $this->contactService->findByName($name);
        if (!$contact) {
            return null;
        }

        $this->provider->dialContact($contact);

        return $this->provider->makeCall();
    }

    /**
     * @param string $number
     * @param string $message
     *
     * @return bool
     */
    public function sendSMS(string $number, string $message): bool
    {
        if (!$message || !$this->contactService->validateNumber($number)) {
            return false;
        }

        // TODO: test max length of SMS if provider doesn't support
        //       splitting messages (inside provider)
        return $this->provider->sendSMS($number, $message);
    }
}
