<?php


namespace App\Interfaces;


use App\Contact;

interface ContactRepositoryInterface
{
    public function findByName(string $name): ?Contact;
}
