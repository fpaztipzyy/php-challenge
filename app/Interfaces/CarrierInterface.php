<?php

namespace App\Interfaces;

use App\Call;
use App\Contact;

/**
 * Interface CarrierInterface
 */
interface CarrierInterface
{
    public function dialContact(Contact $contact);

    public function makeCall(): Call;

    public function sendSMS(string $number, string $message): bool;
}
