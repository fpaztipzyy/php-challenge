<?php

namespace Tests;

use App\Call;
use App\Contact;
use App\Interfaces\CarrierInterface;
use App\Mobile;
use App\Services\ContactService;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class MobileTest extends TestCase
{

    /** @test */
    public function it_returns_null_when_name_empty()
    {
        $provider = m::mock(CarrierInterface::class);
        $repo     = m::mock(ContactService::class);
        $mobile   = new Mobile($provider, $repo);

        $this->assertNull($mobile->makeCallByName(''));
    }

    /** @test */
    public function it_returns_valid_contact()
    {
        $provider = m::mock(CarrierInterface::class);
        $service  = m::mock(ContactService::class);
        $mobile   = new Mobile($provider, $service);

        $contact = m::mock(Contact::class);
        $call    = new Call();

        $service->shouldReceive('findByName')
            ->with('test')
            ->andReturn($contact);

        $provider->shouldReceive('dialContact')
            ->with($contact);

        $provider->shouldReceive('makeCall')
            ->andReturn($call);

        $callR = $mobile->makeCallByName('test');
        $this->assertInstanceOf(Call::class, $callR);
    }

    /** @test */
    public function it_doesnt_find_a_contact()
    {
        $provider = m::mock(CarrierInterface::class);
        $service  = m::mock(ContactService::class);
        $mobile   = new Mobile($provider, $service);

        $service->shouldReceive('findByName')
            ->with('test')
            ->andReturn(null);

        $this->assertNull($mobile->makeCallByName('test'));
    }

    /**
     * @test
     * @dataProvider invalid_phones_provider
     *
     * @param string $number
     * @param string $message
     */
    public function invalid_params_to_send_sms(string $number, string $message)
    {
        $provider = m::mock(CarrierInterface::class);
        $service  = m::mock(ContactService::class);
        $mobile   = new Mobile($provider, $service);

        $service->shouldReceive('validateNumber')
            ->andReturn($number === '912312312');

        $this->assertFalse($mobile->sendSMS($number, $message));
    }

    /**
     * @test
     */
    public function it_sends_sms()
    {
        $provider = m::mock(CarrierInterface::class);
        $service  = m::mock(ContactService::class);
        $mobile   = new Mobile($provider, $service);

        $provider->shouldReceive('sendSMS')
            ->andReturn(true);
        $service->shouldReceive('validateNumber')
            ->andReturn(true);

        $this->assertTrue($mobile->sendSMS('912312312', 'Test message'));
    }

    public function invalid_phones_provider(): array
    {
        return [
            ['000000000', 'Message'],
            ['912312312', ''],
        ];
    }
}
